config.load_autoconfig(False)
# UI preferences
c.editor.command = ["$TERM", "-e", "$EDITOR", "{file}", "-c", "normal {line}G{column0}l"]
c.url.start_pages = 'about:blank'
c.url.default_page = 'about:blank'
c.input.insert_mode.auto_load = False
c.tabs.background = True
#c.tabs.show = 'switching'
c.url.open_base_url = True
# please use a dedicated pdf viewer, like zathura or mupdf
c.content.pdfjs = False
c.content.autoplay = False
# This does not seem to work? I don't use the mouse anyway.
#c.input.mouse.rocker_gestures = True

# darkmode config
c.colors.webpage.darkmode.policy.images = 'never'
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.darkmode.policy.page = 'always'
#c.colors.webpage.darkmode.grayscale.all = True
# If you want dark elements to have less contrast, uncomment this
#c.colors.webpage.darkmode.contrast = -0.03
#c.colors.webpage.darkmode.algorithm = 'lightness-hsl'

# solarized css
c.content.user_stylesheets = [
    'solarized-dark-all-sites.css',
]

c.url.searchengines = {"DEFAULT": "https://gigablast.com/search?c=main&qlangcountry=en-us&q={}",
	"aw": "https://wiki.archlinux.org/index.php?search={}&title=Special%3ASearch&profile=default&fulltext=1",
	"brv": "https://search.brave.com/search?q={}",
	"ddg": "https://lite.duckduckgo.com/lite/{}",
# youtube sucks
#	"yt": "https://yewtu.be/search?q={}",
	"ngm": "https://www.nigma.net.ru/en/index.php?query={}",
	"srx": "https://searx.tiekoetter.com/search?q={}&category_general=on&language=en-US&time_range=&safesearch=0&theme=simple",
	"cdb": "https://codeberg.org/explore/repos?sort=recentupdate&language=&q={}",
	"ksl": "https://classifieds.ksl.com/search?keyword={}",
	"ig": "https://infogalactic.com/w/index.php?search={}",
	"st": "https://simplytranslate.org/?engine=deepl{}",
	"wb": "https://wiby.me/?q={}",
	"mjk": "https://www.mojeek.com/search?q={}",
	"tld": "https://tilde.wtf/search?q={}",
	"tlv": "https://tilvids.com/search?search={}&searchTarget=local",
# openbsd-specific
	"obspa": "https://marc.info/?l=openbsd-ports&w=2&r=1&s={}&q=b",
	"bsdp": "https://openports.se/search.php?stype=folder&so={}",
}

# settings for low-end devices
# this option works on the current version. If you run openbsd stable comment this out and uncomment the option below. Also, it breaks private browsing so you should use my wrapper script instead.
#c.qt.chromium.process_model = "single-process" 
#c.qt.process_model = "single-process" 
c.content.print_element_backgrounds = False
# in case you want to try it without GPU acceleration
#c.qt.force_software_rendering = "software-opengl"
# this sandbox broke my browser on my Void musl laptop back when I used Linux
#c.qt.args = ["disable-seccomp-filter-sandbox"]
# jpeg mode, very funny
#c.qt.low_end_device_mode = 'always'

# bindings 
config.bind('pt', 'tab-pin')
config.bind(';w','hint links spawn --output-messages yt-dlp -quiet --progress -P ~/media/video/playlist {hint-url}')
config.bind(';g','hint links spawn git clone {hint-url}')
config.bind(';d','hint links spawn --output-messages ftp {hint-url}')
config.bind(';W','spawn --output-messages yt-dlp -quiet --progress -P ~/media/video/playlist {url}')
config.bind(';i','config-source i2pconfig.py')
# The output of this userscript sucks, and the better one requires you to use node, which sucks 
# config.bind(',','spawn --userscript readability')
# The "better" one
# config.bind(',','spawn --userscript readability-js')

# password management 
#config.bind('eu','spawn --userscript qute-pass --username-only')
#config.bind('ep','spawn --userscript qute-pass --password-only')
#config.bind('eo','spawn --userscript qute-pass --otp-only')

c.qt.chromium.process_model = "process-per-site-instance" 
# security & privacy settings
c.content.webgl = False
c.content.dns_prefetch = False
c.content.canvas_reading = False
c.content.xss_auditing = True
# "Do not track" is actually used to track you. Ironic, I know.
c.content.headers.do_not_track = False
# We leave JS enabled because Jmatrix blocks it 
c.content.javascript.enabled = True
# If you want privacy, you don't want anything web stored on your computer. No history, no cache, nothing. Take notes and make additional search engines. Quickmarks, bookmarks and cookies get deleted after a restart assuming you're using my wrapper script
c.content.cache.appcache = False
c.content.local_storage = False
c.completion.web_history.max_items = 0
# Cookies are blocked by Jmatrix, we leave them semi-enabled. 
c.content.cookies.accept = "no-3rdparty"
# Private browsing does not work in single-process mode
c.content.private_browsing = True
# Why can't I just disable it, The-Compiler?
c.content.webrtc_ip_handling_policy = "disable-non-proxied-udp"
# adblock
c.content.blocking.enabled = True
c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://easylist.to/easylist/fanboy-social.txt",
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt",
        "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt",
        "https://gitlab.com/curben/urlhaus-filter/-/raw/master/urlhaus-filter.txt",
        "https://pgl.yoyo.org/adservers/serverlist.php?showintro=0;hostformat=hosts",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/legacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2021.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badware.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/badlists.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/resource-abuse.txt",
	"https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-firstparty.txt",
	"https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-social.txt",
	"https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-specific.txt",
	"https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-sugarcoat.txt",
	"https://raw.githubusercontent.com/brave/adblock-lists/master/brave-lists/brave-firstparty-cname.txt",
        "https://www.i-dont-care-about-cookies.eu/abp/",
        "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt",
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt"]
#c.content.blocking.whitelist = ['']
# jmatrix init
import sys, os
sys.path.append(os.path.join(sys.path[0], 'jmatrix'))
config.source("jmatrix/integrations/qutebrowser.py")

# base16-qutebrowser (https://github.com/theova/base16-qutebrowser)
# Base16 qutebrowser template by theova
# Solarized Dark scheme by Ethan Schoonover (modified by aramisgithub)

base00 = "#002b36"
base01 = "#073642"
base02 = "#586e75"
base03 = "#657b83"
base04 = "#839496"
base05 = "#93a1a1"
base06 = "#eee8d5"
base07 = "#fdf6e3"
base08 = "#dc322f"
base09 = "#cb4b16"
base0A = "#b58900"
base0B = "#859900"
base0C = "#2aa198"
base0D = "#268bd2"
base0E = "#6c71c4"
base0F = "#d33682"

# set qutebrowser colors

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
c.colors.completion.fg = base05

# Background color of the completion widget for odd rows.
c.colors.completion.odd.bg = base01

# Background color of the completion widget for even rows.
c.colors.completion.even.bg = base00

# Foreground color of completion widget category headers.
c.colors.completion.category.fg = base0A

# Background color of the completion widget category headers.
c.colors.completion.category.bg = base00

# Top border color of the completion widget category headers.
c.colors.completion.category.border.top = base00

# Bottom border color of the completion widget category headers.
c.colors.completion.category.border.bottom = base00

# Foreground color of the selected completion item.
c.colors.completion.item.selected.fg = base05

# Background color of the selected completion item.
c.colors.completion.item.selected.bg = base02

# Top border color of the selected completion item.
c.colors.completion.item.selected.border.top = base02

# Bottom border color of the selected completion item.
c.colors.completion.item.selected.border.bottom = base02

# Foreground color of the matched text in the selected completion item.
c.colors.completion.item.selected.match.fg = base0B

# Foreground color of the matched text in the completion.
c.colors.completion.match.fg = base0B

# Color of the scrollbar handle in the completion view.
c.colors.completion.scrollbar.fg = base05

# Color of the scrollbar in the completion view.
c.colors.completion.scrollbar.bg = base00

# Background color of disabled items in the context menu.
c.colors.contextmenu.disabled.bg = base01

# Foreground color of disabled items in the context menu.
c.colors.contextmenu.disabled.fg = base04

# Background color of the context menu. If set to null, the Qt default is used.
c.colors.contextmenu.menu.bg = base00

# Foreground color of the context menu. If set to null, the Qt default is used.
c.colors.contextmenu.menu.fg =  base05

# Background color of the context menu’s selected item. If set to null, the Qt default is used.
c.colors.contextmenu.selected.bg = base02

#Foreground color of the context menu’s selected item. If set to null, the Qt default is used.
c.colors.contextmenu.selected.fg = base05

# Background color for the download bar.
c.colors.downloads.bar.bg = base00

# Color gradient start for download text.
c.colors.downloads.start.fg = base00

# Color gradient start for download backgrounds.
c.colors.downloads.start.bg = base0D

# Color gradient end for download text.
c.colors.downloads.stop.fg = base00

# Color gradient stop for download backgrounds.
c.colors.downloads.stop.bg = base0C

# Foreground color for downloads with errors.
c.colors.downloads.error.fg = base08

# Font color for hints.
c.colors.hints.fg = base00

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
c.colors.hints.bg = base0A

# Font color for the matched part of hints.
c.colors.hints.match.fg = base05

# Text color for the keyhint widget.
c.colors.keyhint.fg = base05

# Highlight color for keys to complete the current keychain.
c.colors.keyhint.suffix.fg = base05

# Background color of the keyhint widget.
c.colors.keyhint.bg = base00

# Foreground color of an error message.
c.colors.messages.error.fg = base00

# Background color of an error message.
c.colors.messages.error.bg = base08

# Border color of an error message.
c.colors.messages.error.border = base08

# Foreground color of a warning message.
c.colors.messages.warning.fg = base00

# Background color of a warning message.
c.colors.messages.warning.bg = base0E

# Border color of a warning message.
c.colors.messages.warning.border = base0E

# Foreground color of an info message.
c.colors.messages.info.fg = base05

# Background color of an info message.
c.colors.messages.info.bg = base00

# Border color of an info message.
c.colors.messages.info.border = base00

# Foreground color for prompts.
c.colors.prompts.fg = base05

# Border used around UI elements in prompts.
c.colors.prompts.border = base00

# Background color for prompts.
c.colors.prompts.bg = base00

# Background color for the selected item in filename prompts.
c.colors.prompts.selected.bg = base02

# Foreground color for the selected item in filename prompts.
c.colors.prompts.selected.fg = base05

# Foreground color of the statusbar.
c.colors.statusbar.normal.fg = base0B

# Background color of the statusbar.
c.colors.statusbar.normal.bg = base00

# Foreground color of the statusbar in insert mode.
c.colors.statusbar.insert.fg = base00

# Background color of the statusbar in insert mode.
c.colors.statusbar.insert.bg = base0D

# Foreground color of the statusbar in passthrough mode.
c.colors.statusbar.passthrough.fg = base00

# Background color of the statusbar in passthrough mode.
c.colors.statusbar.passthrough.bg = base0C

# Foreground color of the statusbar in private browsing mode.
c.colors.statusbar.private.fg = base00

# Background color of the statusbar in private browsing mode.
c.colors.statusbar.private.bg = base01

# Foreground color of the statusbar in command mode.
c.colors.statusbar.command.fg = base05

# Background color of the statusbar in command mode.
c.colors.statusbar.command.bg = base00

# Foreground color of the statusbar in private browsing + command mode.
c.colors.statusbar.command.private.fg = base05

# Background color of the statusbar in private browsing + command mode.
c.colors.statusbar.command.private.bg = base00

# Foreground color of the statusbar in caret mode.
c.colors.statusbar.caret.fg = base00

# Background color of the statusbar in caret mode.
c.colors.statusbar.caret.bg = base0E

# Foreground color of the statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.fg = base00

# Background color of the statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.bg = base0D

# Background color of the progress bar.
c.colors.statusbar.progress.bg = base0D

# Default foreground color of the URL in the statusbar.
c.colors.statusbar.url.fg = base05

# Foreground color of the URL in the statusbar on error.
c.colors.statusbar.url.error.fg = base08

# Foreground color of the URL in the statusbar for hovered links.
c.colors.statusbar.url.hover.fg = base05

# Foreground color of the URL in the statusbar on successful load
# (http).
c.colors.statusbar.url.success.http.fg = base0C

# Foreground color of the URL in the statusbar on successful load
# (https).
c.colors.statusbar.url.success.https.fg = base0B

# Foreground color of the URL in the statusbar when there's a warning.
c.colors.statusbar.url.warn.fg = base0E

# Background color of the tab bar.
c.colors.tabs.bar.bg = base00

# Color gradient start for the tab indicator.
c.colors.tabs.indicator.start = base0D

# Color gradient end for the tab indicator.
c.colors.tabs.indicator.stop = base0C

# Color for the tab indicator on errors.
c.colors.tabs.indicator.error = base08

# Foreground color of unselected odd tabs.
c.colors.tabs.odd.fg = base05

# Background color of unselected odd tabs.
c.colors.tabs.odd.bg = base01

# Foreground color of unselected even tabs.
c.colors.tabs.even.fg = base05

# Background color of unselected even tabs.
c.colors.tabs.even.bg = base00

# Background color of pinned unselected even tabs.
c.colors.tabs.pinned.even.bg = base0C

# Foreground color of pinned unselected even tabs.
c.colors.tabs.pinned.even.fg = base07

# Background color of pinned unselected odd tabs.
c.colors.tabs.pinned.odd.bg = base0B

# Foreground color of pinned unselected odd tabs.
c.colors.tabs.pinned.odd.fg = base07

# Background color of pinned selected even tabs.
c.colors.tabs.pinned.selected.even.bg = base02

# Foreground color of pinned selected even tabs.
c.colors.tabs.pinned.selected.even.fg = base05

# Background color of pinned selected odd tabs.
c.colors.tabs.pinned.selected.odd.bg = base02

# Foreground color of pinned selected odd tabs.
c.colors.tabs.pinned.selected.odd.fg = base05

# Foreground color of selected odd tabs.
c.colors.tabs.selected.odd.fg = base05

# Background color of selected odd tabs.
c.colors.tabs.selected.odd.bg = base02

# Foreground color of selected even tabs.
c.colors.tabs.selected.even.fg = base05

# Background color of selected even tabs.
c.colors.tabs.selected.even.bg = base02

# Background color for webpages if unset (or empty to use the theme's
# color).
c.colors.webpage.bg = base00

